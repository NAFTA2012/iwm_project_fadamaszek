package fakeSensors;

//placeholder for quick testing without real input
public abstract class FakeSensors {
    //returns array of random values used for testing
    public static double[] getReadings(int amount, int steps){
        double[] readings = new double[amount];

        for(int i = 0; i < readings.length; i++){
            readings[i] = Math.floor(Math.random()*steps)/steps;
        }

        return readings;
    }
}