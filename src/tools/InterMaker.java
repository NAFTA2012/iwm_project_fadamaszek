package tools;

import dataTypes.*;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

//tool form making custom 3 and 4 way intersections with custom states
public class InterMaker {
    private enum Status{//status of program
        EXIT,       //end program
        ROADS1,     //select intersection type
        ROADS2,     //select legal maneuvers
        STATES1,    //select relevant road for state
        STATES2,    //select legal maneuvers (green lights) for state
        STATES3,    //define state parameters
        STATES4,    //define relevant sensors
        COMBINE,    //create intersection object
        EXPORT;     //export intersection to file
    }

    public static void main(String[] args){
        Status status = Status.ROADS1;                      //program status
        Scanner scanner = null;                             //scanner for getting human input
        String line = "";                                   //command inserted by human
        Directions[] roads = null;                          //temporary legal maneuvers
        RoadState[] inRoads = null;                         //intersection roads definitions
        Directions direction = Directions.roadNONE;         //road selection value
        Turns[] turns = null;                               //green lights selection
        List<RoadState> interStateList = new ArrayList<>(); //roads with green lights defined for edited state
        double time = 0.0;                                  //basic time length of state
        double extendTime = 0.0;                            //additional time length of state
        double extendTrsh = 1.0;                            //threshold triggering additional time length of state
        int maxExtends = 0;                                 //how many time additional time can be triggered
        List<Integer> sensors = new ArrayList<>();          //IDs of sensors, intermediate variable
        int[] sensorIDs = new int[]{-1};                    //IDs of sensors that can extend time length of state
        List<State> states = new ArrayList<>();             //states of currently edited intersection
        Intersection intersection = null;                   //variable for new intersection

        try {
            scanner = new Scanner(System.in);

            //main program loop
            while (status != Status.EXIT) {
                //print info
                switch (status) {
                    case ROADS1:
                        System.out.print("Input number of roads (3 or 4): ");
                        break;

                    case ROADS2:
                        System.out.println("Input allowed maneuvers fo reach input: ");
                        break;

                    case STATES1:
                        System.out.print("Input road for state or N to setup properties and finish state or E to finish intersection or X to exit: ");
                        break;

                    case STATES2:
                        System.out.print("Input allowed maneuvers: ");
                        break;

                    case STATES3:
                        System.out.println("Insert time(double > 0) [enter] extendTime(double >= 0) [enter] extendTrsh(0 < double <= 1) [enter] maxExtends(int >= 0) [enter]:");
                        break;

                    case STATES4:
                        System.out.println("Insert dependent sensors, press [enter] to insert sensor, type -1 to finish:");
                        break;

                    case COMBINE:
                        System.out.println("Generating intersection:");
                        break;

                    case EXPORT:
                        if(args.length == 0) {
                            System.out.print("Insert file name or leave empty to discard and make new intersection: ");
                        } else {
                            System.out.print("Insert anything or leave empty to discard and make new intersection: ");
                        }
                        break;
                }

                //interpret input
                switch (status) {
                    case ROADS1 -> {
                        int ins = Integer.parseInt(userInput(scanner));
                        switch (ins) {
                            case 3 -> {
                                roads = new Directions[]{Directions.roadA, Directions.roadB, Directions.roadC};
                                status = Status.ROADS2;
                            }
                            case 4 -> {
                                roads = new Directions[]{Directions.roadA, Directions.roadB, Directions.roadC, Directions.roadD};
                                status = Status.ROADS2;
                            }
                            default -> System.out.println("Invalid input");
                        }
                    }
                    case ROADS2 -> {
                        inRoads = new RoadState[roads.length];
                        for (int i = 0; i < roads.length; i++) {
                            do {
                                System.out.print(roads[i] + ": ");
                                turns = Turns.fromString(userInput(scanner));

                                if (turns[0] == Turns.NONE) {
                                    System.out.print("Wrong turn/s, ");
                                }
                            } while (turns[0] == Turns.NONE);

                            {
                                inRoads[i] = new RoadState(roads[i], turns);
                            }
                        }
                        status = Status.STATES1;
                    }
                    case STATES1 -> {
                        line = userInput(scanner);
                        switch (line.toLowerCase()) {
                            case "n" -> status = Status.STATES3;
                            case "e" -> status = Status.COMBINE;
                            case "x" -> status = Status.EXIT;
                            default -> {
                                direction = Directions.fromString(line);
                                if (direction == Directions.roadNONE) {
                                    System.out.println("Wrong direction");
                                } else {
                                    status = Status.STATES2;
                                }
                            }
                        }
                    }
                    case STATES2 -> {
                        turns = Turns.fromString(userInput(scanner));
                        if (turns[0] == Turns.NONE) {
                            System.out.println("Wrong turn/s");
                        } else {
                            interStateList.add(new RoadState(direction, turns));
                            status = Status.STATES1;
                        }
                    }
                    case STATES3 -> {
                        time = Double.parseDouble(userInput(scanner));
                        extendTime = Double.parseDouble(userInput(scanner));
                        extendTrsh = Double.parseDouble(userInput(scanner));
                        maxExtends = Integer.parseInt(userInput(scanner));
                        status = Status.STATES4;
                    }
                    case STATES4 -> {
                        line = userInput(scanner);
                        if (line.equals("-1")) {
                            sensors.add(Integer.parseInt(line));
                        } else {
                            while (!line.equals("-1")) {
                                sensors.add(Integer.parseInt(line));
                                line = userInput(scanner);
                            }
                        }
                        sensorIDs = new int[sensors.size()];
                        for (int i = 0; i < sensorIDs.length; i++) {
                            sensorIDs[i] = sensors.get(i);
                        }
                        states.add(new State(interStateList.toArray(new RoadState[interStateList.size()]), time, extendTime, extendTrsh, maxExtends, sensorIDs));
                        sensors.clear();
                        interStateList.clear();
                        status = Status.STATES1;
                    }
                    case COMBINE -> {
                        intersection = new Intersection(inRoads, states.toArray(new State[states.size()]));
                        System.out.println(intersection);
                        status = Status.EXPORT;
                    }
                    case EXPORT -> {
                        line = userInput(scanner);
                        if (!line.equals("") && intersection != null) {
                            String fileName = "";
                            if (args.length == 0) {
                                fileName = line + ".itr";
                            } else {
                                fileName = args[0] + ".itr";
                            }
                            OutputStream os = new FileOutputStream(fileName);
                            os.write(Objects.requireNonNull(DataTools.serialize(intersection)));
                            os.close();
                            status = Status.EXIT;
                        } else {
                            status = Status.ROADS1;
                        }
                    }
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            assert scanner != null;
            scanner.close();
        }

    }

    //read user input
    public static String userInput(Scanner scanner){
        if (scanner.hasNextLine())
            return scanner.nextLine();
        else
            return "";
    }
}