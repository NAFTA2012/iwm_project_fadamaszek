package server;

import dataTypes.Intersection;
import dataTypes.Message;
import dataTypes.ServerData;
import dataTypes.State;
import tools.DataTools;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Objects;

public class Server {
    public static void main(String[] args){
        //check if enough input arguments
        if (args.length < 1) {
            System.out.println("Usage: Server <server data fileName>");
            System.exit(-1);
        }

        ServerData serverData = ServerData.loadFromFile(args[0]);   //retrieve information about server

        DatagramSocket aSocket = null;      //socket for data transfer
        Intersection intersection = null;   //currently analyzed intersection
        int ID = -1;                        //ID of currently analyzed intersection
        double[] sensors;                   //sensors of currently analyzed intersection
        int[] combData;                     //combined data array of currently analyzed intersection
        int nextState;                      //computed best next intersection state

        try {
            //define data transfer socket
            aSocket = new DatagramSocket(9876);
            byte[] buffer = new byte[65535];
            byte[] outBuffer = new byte[65535];
            String feedback = "";

            //server main loop
            while (true) {
                DatagramPacket request = new DatagramPacket(buffer, buffer.length); //packet for received message
                DatagramPacket reply = null;                                        //packet for sending message
                System.out.println("Waiting for request...");
                aSocket.receive(request);
                System.out.println("Message received");

                Message<?,?> read = (Message<?,?>) DataTools.deserialize(request.getData());
                Message answer = null;

                //process message
                switch (read.getMsgType()) {
                    case NEWINTER -> {
                        System.out.println("Adding new intersection");
                        intersection = (Intersection) read.getMsgA();
                        assert serverData != null;
                        ID = serverData.addIntersection(intersection);
                        ServerData.saveToFile(args[0], serverData);
                        System.out.println("New intersection added, intersection ID: " + ID);
                        answer = new Message(Message.MsgType.JOINED, 0, ID);
                    }
                    case UPDATEINTER -> {
                        intersection = (Intersection) read.getMsgA();
                        ID = (int) read.getMsgB();
                        System.out.println("Intersection update requested, intersection ID: " + ID);
                        switch (Objects.requireNonNull(serverData).updateIntersection(ID, intersection)) {
                            case SUCCESS -> {
                                System.out.println("Intersection update complete, intersection ID: " + ID);
                                ServerData.saveToFile(args[0], serverData);
                                answer = new Message(Message.MsgType.UPDATED, 0, ID);
                            }
                            case FAIL -> {
                                System.out.println("Adding new intersection");
                                intersection = (Intersection) read.getMsgA();
                                ID = serverData.addIntersection(intersection);
                                ServerData.saveToFile(args[0], serverData);
                                System.out.println("Intersection update failed, intersection joined: " + ID);
                                answer = new Message(Message.MsgType.JOINED, 0, ID);
                            }
                        }
                    }
                    case ORDERREQUEST -> {
                        sensors = (double[]) read.getMsgA();
                        combData = (int[]) read.getMsgB();
                        ID = combData[Message.CombData.ID.index];
                        System.out.println("Next order requested, intersection ID: " + ID);
                        assert serverData != null;
                        intersection = serverData.getIntersection(ID);
                        nextState = calculateAction(intersection, sensors, combData);
                        if (nextState == -1) {
                            System.out.println("Repeat command send, intersection ID: " + ID);
                            answer = new Message(Message.MsgType.REPEATSTATE, nextState, ID);
                        } else if (nextState == combData[Message.CombData.STATE.index]) {
                            System.out.println("Extend command send, intersection ID: " + ID + "\t, state: " + nextState);
                            answer = new Message(Message.MsgType.EXTENDSTATE, nextState, ID);
                        } else if (intersection.getStates()[nextState].getTime() > 0.0) {
                            System.out.println("Set command send, intersection ID: " + ID + "\t, state: " + nextState);
                            answer = new Message(Message.MsgType.SETSTATE, nextState, ID);
                        } else {
                            System.out.println("Set and extend command send, intersection ID: " + ID + "\t, state: " + nextState);
                            answer = new Message(Message.MsgType.SETEXTENDSTATE, nextState, ID);
                        }
                    }
                    default -> {
                        System.out.println("Unexpected message received");
                        answer = new Message(Message.MsgType.FAIL, read.getMsgType(), -1);
                    }
                }
                //send message
                outBuffer = DataTools.serialize(answer);
                assert outBuffer != null;
                reply = new DatagramPacket(outBuffer, outBuffer.length, request.getAddress(), request.getPort());
                aSocket.send(reply);
            }

        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        } finally {
            assert aSocket != null;
            aSocket.close();
        }
    }

    //calculate next intersection state
    private static int calculateAction(Intersection intersection, double[] sensors, int[] combData) {
        State[] states = intersection.getStates();          //states of current intersection
        int check = combData[Message.CombData.STATE.index]; //ID of state that is currently being checked
        State state = states[check];                        //state that is currently being checked
        int[] sensorIDs = state.getSensorIDs();             //ID of sensors relevant to currently checked state

        //check if current state can be extended
        if((sensorIDs[0] != -1) && (state.getMaxExtends() > combData[Message.CombData.EXTENSIONS.index])) {
            for (int sensorID : sensorIDs) {            //check relevant sensors
                //System.out.println(i + "A: " + sensors[sensorIDs[i]] + "\t" + state.getExtendTrsh());
                if (sensors[sensorID] > state.getExtendTrsh())  //check is sensor is above threshold
                    return check;                                   //if found give information to keep state
            }
        }

        //find next state that can be enabled
        for(int i = 1; i < states.length; i++) {
            check = (i + combData[Message.CombData.STATE.index])%states.length;
            //System.out.println(check);
            if(states[check].getTime() > 0.0){  //check if state has to be enabled regardless of sensors
                return check;                   //if yes, give information to use this state
            } else {                            //check if state has to be enabled based on relevant sensors
                state = states[check];
                sensorIDs = state.getSensorIDs();
                if((sensorIDs[0] != -1) && (state.getMaxExtends() > 0)) {   //check if sensors can force enabling state
                    for (int sensorID : sensorIDs) {            //check relevant sensors
                        //System.out.println(j + "B: " + sensors[sensorIDs[j]] + "\t" + state.getExtendTrsh());
                        if (sensors[sensorID] > state.getExtendTrsh())  //check if sensor is above threshold
                            return check;                                   //if found give information to change state
                    }
                }
            }
        }

        return -1;  //no next viable state was found so repeat current one
    }
}