package dataTypes;

import java.io.Serializable;

//legal maneuvers
public enum Turns implements Serializable {
    NONE,
    F,
    R,
    L;

    //convert string to array of legal maneuvers
    public static Turns[] fromString(String s) {
        return switch (s.toLowerCase()) {
            case "f" -> new Turns[]{F};
            case "r" -> new Turns[]{R};
            case "l" -> new Turns[]{L};
            case "fr", "rf" -> new Turns[]{F, R};
            case "fl", "lf" -> new Turns[]{F, L};
            case "rl", "lr" -> new Turns[]{R, L};
            case "frl", "flr", "rfl", "rlf", "lfr", "lrf" -> new Turns[]{F, R, L};
            default -> new Turns[]{NONE};
        };
    }
}