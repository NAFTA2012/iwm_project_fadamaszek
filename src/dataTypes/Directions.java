package dataTypes;

import java.io.Serializable;

//legal road directions
public enum Directions implements Serializable {
    roadNONE,
    roadA,
    roadB,
    roadC,
    roadD;

    //convert string to direction
    public static Directions fromString(String s){
        return switch (s.toLowerCase()) {
            case "a", "roada" -> roadA;
            case "b", "roadb" -> roadB;
            case "c", "roadc" -> roadC;
            case "d", "roadd" -> roadD;
            default -> roadNONE;
        };
    }
}