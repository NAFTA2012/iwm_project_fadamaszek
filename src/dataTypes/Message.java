package dataTypes;

import java.io.Serializable;

//message structure, defines how client and server communicate
public class Message<T,U> implements Serializable {
    private final MsgType msgType;    //type of send message
    private final T msgA;             //first part of message
    private final U msgB;             //second part of message

    //recipe for integer array combining data in strict manner
    public enum CombData implements Serializable{
        STATE(0),       //state of intersection
        EXTENSIONS(1),  //extensions of given state
        ID(2);          //ID of intersection

        public int index;   //index of data in array

        CombData(int index) {
            this.index = index;
        }
    }
    //types of messages send
    public enum MsgType implements Serializable{
        NEWINTER,       //client, register new intersection
        UPDATEINTER,    //client, update existing intersection
        ORDERREQUEST,   //client, advise next step from server
        JOINED,         //server, inform that adding intersection succeeded and give new ID
        UPDATED,        //server, inform that updating intersection succeeded
        SETSTATE,       //server, order to set given state
        EXTENDSTATE,    //server, order to extend current state
        SETEXTENDSTATE, //server, order to set given state but on extension rules
        REPEATSTATE,    //server, order to repeat state
        FAIL;           //server, inform that something went wrong
    }

    //create message
    public Message(MsgType msgType,T msgA, U msgB){
        this.msgType = msgType;
        this.msgA = msgA;
        this.msgB = msgB;
    }

    public T getMsgA() {
        return msgA;
    }

    public U getMsgB() {
        return msgB;
    }

    public MsgType getMsgType() {
        return msgType;
    }

    @Override
    public String toString() {
        return "msgType: " + msgType + "\n";
    }
}