package dataTypes;

import java.io.Serializable;

public class State implements Serializable {
    protected double time;          //base time length of state
    protected double extendTime;    //additional time length of state
    protected double extendTrsh;    //threshold triggering additional time length of state
    protected int maxExtends;       //how many time additional time can be triggered
    protected int[] sensorIDs;      //IDs of sensors that can extend time length of state
    protected RoadState[] roads;    //legal maneuvers (green lights) during state

    //basic state without sensors
    public State(RoadState[] roads, double time){
        this.time = time;
        this.extendTime = 0.0;
        this.extendTrsh = 1.0;
        this.maxExtends = 0;
        this.sensorIDs = new int[]{-1};
        this.roads = roads;
    }

    //custom state
    public State(RoadState[] roads, double time, double extendTime, double extendTrsh, int maxExtends, int[] sensorsID){
        this.time = time;
        this.extendTime = extendTime;
        this.extendTrsh = extendTrsh;
        this.maxExtends = maxExtends;
        this.sensorIDs = sensorsID;
        this.roads = roads;
    }

    @Override
    public String toString() {
        StringBuilder outString = new StringBuilder("Single state\n\ttime:\t\t\t\t" + time + "\n\textend time:\t\t" + extendTime
                + "\n\textend threshold:\t" + extendTrsh + "\n\tmax extends:\t\t" + maxExtends + "\n\tsensor ID:\t\t\t");

        for (int sensorID : sensorIDs) {
            outString.append(sensorID).append(" ");
        }

        for (RoadState road : roads) {
            outString.append("\n\t").append(road.toString());
        }

        return outString + "\n";
    }

    //string of only allowed maneuvers
    public String stateToString() {
        StringBuilder outString = new StringBuilder();
        for (RoadState road : roads) {
            outString.append("\t").append(road.toString());
        }
        return outString.toString();
    }

    public double getExtendTime() {
        return extendTime;
    }

    public double getExtendTrsh() {
        return extendTrsh;
    }

    public double getTime() {
        return time;
    }

    public int getMaxExtends() {
        return maxExtends;
    }

    public RoadState[] getRoads() {
        return roads;
    }

    public int[] getSensorIDs() {
        return sensorIDs;
    }
}