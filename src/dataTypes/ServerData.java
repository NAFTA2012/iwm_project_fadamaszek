package dataTypes;

import tools.DataTools;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

//structure containing data used by server
public class ServerData implements Serializable {
    private Integer nextID;                     //ID of next new intersection
    private List<Integer> intersectionIDs;      //list of IDs of intersections registered to server
    private List<Intersection> intersections;   //list of intersections registered to server

    //empty ServerData constructor
    public ServerData() {
        nextID = 0;
        intersectionIDs = new ArrayList<>();
        intersections = new ArrayList<>();
    }

    //add intersection to server data
    public int addIntersection(Intersection newIntersection){
        Integer newID = nextID;
        nextID++;
        newIntersection.setID(newID);
        intersections.add(newIntersection);
        intersectionIDs.add(newID);
        return newID;
    }

    //retrieve intersection data of given ID
    public Intersection getIntersection(int ID){
        for(int i = 0; i < intersectionIDs.size(); i++){
            if(ID == intersectionIDs.get(i))
                return intersections.get(i);
        }
        return null;
    }

    //update intersection data of given ID
    public Act updateIntersection(int ID, Intersection intersection) {
        for(int i = 0; i < this.intersectionIDs.size(); i++){
            System.out.println("interID\t" + this.intersectionIDs.get(i));
            if(ID == this.intersectionIDs.get(i)) {
                this.intersections.set(i,intersection);
                return Act.SUCCESS;
            }
        }
        return Act.FAIL;
    }

    //save server data to file
    public static void saveToFile(String fileName, ServerData serverData) {
        try {
            OutputStream os = new FileOutputStream(fileName + ".srd");
            os.write(Objects.requireNonNull(DataTools.serialize(serverData)));
            System.out.println("ServerData file saved");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //load server data from file, if not possible create file
    public static ServerData loadFromFile(String fileName) {
        File readability = new File(fileName + ".srd");

        //load file
        if (readability.isFile() && readability.canRead()) {
            try {
                InputStream is = new FileInputStream(fileName + ".srd");
                System.out.println("ServerData file loaded");
                return (ServerData) DataTools.deserialize(is.readAllBytes());
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else {    //if not possible create new file
            System.out.println("File not found, creating new serverData file");
            ServerData serverData = new ServerData();
            saveToFile(fileName, serverData);
            return serverData;
        }

        return null;
    }
}