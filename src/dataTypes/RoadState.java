package dataTypes;

import java.io.Serializable;

//state of single road connected to intersection
//can be used to define legal maneuvers or green lights for road
public class RoadState implements Serializable {
    protected Directions direction; //direction of road
    protected Turns[] turns;        //allowed maneuvers

    //constructor with all maneuvers legal
    public RoadState(Directions direction){
        this.direction = direction;
        this.turns = new Turns[]{Turns.F,Turns.R,Turns.L};
    }

    //constructor with selected maneuvers legal
    public RoadState(Directions direction, Turns[] turn){
        this.direction = direction;
        this.turns = turn;
    }

    @Override
    public String toString() {
        StringBuilder outString = new StringBuilder(direction.toString() + "-");

        for (Turns turn : turns) {
            outString.append(turn.toString());
        }

        return outString.toString();
    }

    public Directions getDirection() {
        return direction;
    }

    public Turns[] getTurns() {
        return turns;
    }
}