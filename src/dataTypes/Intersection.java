package dataTypes;

import java.io.Serializable;

public class Intersection implements Serializable {
    private RoadState[] roads;  //roads directions and allowed maneuvers
    private State[] states;     //gives information about green lights in each defined state
    private int sensors;        //total amount of sensors on intersection
    private int ID;             //ID of intersection, -1 if not assigned yet

    //basic 4 way intersection constructor
    public Intersection() {
        roads = new RoadState[]{new RoadState(Directions.roadA,new Turns[]{Turns.F,Turns.R,Turns.L}),
                new RoadState(Directions.roadB,new Turns[]{Turns.F,Turns.R,Turns.L}),
                new RoadState(Directions.roadC,new Turns[]{Turns.F,Turns.R,Turns.L}),
                new RoadState(Directions.roadD,new Turns[]{Turns.F,Turns.R,Turns.L})};

        states = new State[]{new State(new RoadState[]{new RoadState(Directions.roadA,new Turns[]{Turns.F,Turns.R,Turns.L}), new RoadState(Directions.roadC,new Turns[]{Turns.F,Turns.R,Turns.L})},20.0),
                new State(new RoadState[]{new RoadState(Directions.roadB,new Turns[]{Turns.F,Turns.R,Turns.L}), new RoadState(Directions.roadD,new Turns[]{Turns.F,Turns.R,Turns.L})},20.0)};
        sensors = 0;
        ID = -1;
    }

    //custom intersection constructor
    public Intersection(RoadState[] roads, State[] states) {
        this.roads = roads;
        this.states = states;
        this.sensors = countSensors();
        this.ID = -1;
    }

    @Override
    public String toString() {
        String outString = "Intersection:";

        for (RoadState road : roads) {
            outString = outString + "\t" + road.toString();
        }

        outString = outString + "\nSensors:\t\t" + sensors + "\nID:\t\t\t\t" + ID + "\n\n";

        for(int i = 0; i < states.length; i++){
            outString = outString + i + ": " + states[i] + "\n";
        }

        return outString;
    }

    public RoadState[] getRoads() {
        return roads;
    }

    public int getSensorCount() {
        return sensors;
    }

    public State[] getStates() {
        return states;
    }

    //count sensors on intersection, based on highest sensor ID
    private int countSensors() {
        int maxID = -1;
        int semiMaxID = -1;

        for (State state : states) {
            for (int j = 0; j < state.sensorIDs.length; j++) {
                if (state.sensorIDs[j] > semiMaxID)
                    semiMaxID = state.sensorIDs[j];
            }
            if (semiMaxID > maxID)
                maxID = semiMaxID;
        }

        return maxID + 1;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}