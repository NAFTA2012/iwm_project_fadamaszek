package client;

import dataTypes.Intersection;
import dataTypes.Message;
import dataTypes.ServerData;
import dataTypes.State;
import fakeSensors.FakeSensors;
import tools.DataTools;
import tools.InterMaker;

import java.io.*;
import java.net.*;
import java.util.Objects;
import java.util.Scanner;

public class Client {
    public static String line = ""; //command inserted by human

    public static void main(String[] args) {
        final int sleepTimeMilisMain = 500;        //main thread refresh rate
        final int sleepTimeMilisCommand = 100;     //command thread refresh rate
        final int fakeSensorSteps = 10;            //fake sensor states amount
        Scanner scanner = new Scanner(System.in);  //scanner for getting human input

        //check if enough input arguments
        if (args.length < 2) {
            System.out.println("Usage: Client <intersection data fileName> <server host name>");
            System.exit(-1);
        }

        System.out.println("Client starting");

        //command thread definition
        Thread commandThread = new Thread(()-> {
            //scanner loop
            while(true) {
                if (scanner.hasNextLine())
                    setLine(scanner.nextLine());
                try {
                    Thread.sleep(sleepTimeMilisCommand);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread mainThread = new Thread(()-> {
            double timer = 0.0;                 //state timer
            boolean runTimer = true;            //is timer running
            int ID;                             //ID of current intersection
            int extensions = 0;                 //number of extensions in given cycle
            int activeState = 0;                //currently active state
            DatagramSocket aSocket = null;      //data sending socket
            DatagramPacket dataPacket = null;   //data sending packet
            DatagramPacket answer = null;       //data receiving packet
            int serverPort = 9876;              //server port
            String aHost = args[1];             //server adress
            byte[] buffer = new byte[65535];    //data input buffer
            byte[] outBuffer;                   //data output buffer
            Message read = null;                //received messagge
            Message send;                       //message to send

            //socket configuration
            try {
                aSocket = new DatagramSocket(9877);
                aSocket.setSoTimeout(5000);
            } catch (SocketException e) {
                e.printStackTrace();
            }

            Intersection intersection = null;                   //current intersection
            intersection = loadIntersection(args[0]);
            ID = intersection.getID();

            System.out.println(intersection);

            if (ID == -1) {
                send = new Message(Message.MsgType.NEWINTER, intersection, ID); //request joining to server
                System.out.println("Sending join request");
            } else {
                send = new Message(Message.MsgType.UPDATEINTER, intersection, ID); //request updating intersection information on server
                System.out.println("Sending update request");
            }

            //sending message
            try {
                outBuffer = DataTools.serialize(send);
                dataPacket = new DatagramPacket(outBuffer, outBuffer.length, InetAddress.getByName(aHost), serverPort);
                assert aSocket != null;
                aSocket.send(dataPacket);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //receiving message
            try {
                System.out.println("Waiting for answer...");
                answer = new DatagramPacket(buffer, buffer.length);
                assert aSocket != null;
                aSocket.receive(answer);
                read = (Message<?,?>) DataTools.deserialize(answer.getData());
            } catch (ClassNotFoundException | IOException e) {
                e.printStackTrace();
            }

            //interpreting message
            switch (Objects.requireNonNull(read).getMsgType()) {
                case JOINED -> {
                    activeState = (int) read.getMsgA();
                    ID = (int) read.getMsgB();
                    intersection.setID(ID);
                    saveIntersection(args[0], intersection);
                    System.out.println("Joined server successfully");
                    sendToController(intersection.getStates()[activeState]);
                }
                case UPDATED -> {
                    activeState = (int) read.getMsgA();
                    if (ID == (int) read.getMsgB()) {
                        activeState = (int) read.getMsgA();
                        sendToController(intersection.getStates()[activeState]);
                        System.out.println("Information on server updated successfully");
                    } else {
                        System.out.println("Something went wrong on server side");
                        System.exit(1);
                    }
                }
                default -> {
                    System.out.println("Unexpected message received");
                    System.exit(1);
                }
            }

            //thread loop
            while(true) {
                //increment timer
                if (runTimer) {
                    timer = timer + ((double) sleepTimeMilisMain) / 1000.0;
                }

                //state finished check
                if (((timer >= intersection.getStates()[activeState].getTime()) && (extensions == 0)) || ((timer >= intersection.getStates()[activeState].getExtendTime()) && (extensions > 0))) {
                    int[] msgIntArray = new int[Message.CombData.values().length];
                    for(int i = 0; i < msgIntArray.length; i++) {
                        switch (Message.CombData.values()[i]) {
                            case STATE -> msgIntArray[Message.CombData.values()[i].index] = activeState;
                            case EXTENSIONS -> msgIntArray[Message.CombData.values()[i].index] = extensions;
                            case ID -> msgIntArray[Message.CombData.values()[i].index] = ID;
                        }
                    }

                    //send order request message
                    try {
                        System.out.println("Sending order request after: " + timer + "s");
                        send = new Message(Message.MsgType.ORDERREQUEST, getSensors(intersection.getSensorCount(),fakeSensorSteps), msgIntArray);
                        outBuffer = DataTools.serialize(send);
                        assert outBuffer != null;
                        dataPacket = new DatagramPacket(outBuffer, outBuffer.length, InetAddress.getByName(aHost), serverPort);
                        aSocket.send(dataPacket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //receive message with orders
                    try {
                        System.out.println("Waiting for answer...");
                        answer = new DatagramPacket(buffer, buffer.length);
                        aSocket.receive(answer);
                        read = (Message<?,?>) DataTools.deserialize(answer.getData());
                    } catch (ClassNotFoundException | IOException e) {
                        e.printStackTrace();
                    }

                    //react to received message
                    if (((int) read.getMsgB()) == ID) {
                        switch (read.getMsgType()) {
                            case SETSTATE -> {
                                extensions = 0;
                                activeState = (int) read.getMsgA();
                                System.out.println("Next state (server)\t\t" + activeState + "\textensions:\t" + extensions);
                            }
                            case EXTENDSTATE -> {
                                extensions++;
                                System.out.println("Extend state (server)\t" + activeState + "\textensions:\t" + extensions);
                            }
                            case REPEATSTATE -> {
                                extensions = 0;
                                System.out.println("Repeat state (server)\t" + activeState + "\textensions:\t" + extensions);
                            }
                            case SETEXTENDSTATE -> {
                                extensions = 1;
                                activeState = (int) read.getMsgA();
                                System.out.println("Next state (server)\t\t" + activeState + "\textensions:\t" + extensions);
                            }
                        }
                    } else {
                        if (extensions == 0) {
                            extensions++;
                            System.out.println("Extend state (client)\t" + activeState + "\textensions:\t" + extensions);
                        } else {
                            activeState = (activeState + 1)%intersection.getStates().length;
                            extensions = 0;
                            System.out.println("Force state (client)\t" + activeState + "\textensions:\t" + extensions);
                        }
                    }

                    sendToController(intersection.getStates()[activeState]);    //send information to lights controller
                    timer = 0.0;
                }

                //handle human input
                if(!getLine().equals("")){
                    switch (getLine().toLowerCase()) {
                        case "p" -> //pause/unpause
                                runTimer = !runTimer;
                        case "r" -> //reset timer
                                timer = 0.0;
                        case "x" -> //exit
                                System.exit(0);
                        default -> { //change intersection state
                            activeState = Integer.parseInt(getLine());
                            timer = 0.0;
                        }
                    }
                    setLine(""); //clear command
                }

                try {
                    Thread.sleep(sleepTimeMilisMain);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        System.out.println("Client ready");
        System.out.println("Insert:\n\tP - pause/unpause\n\tR - reset timer\n\tX - exit\n\tInteger - force state");
        //start threads
        mainThread.start();
        commandThread.start();
    }

    //load intersection from file
    public static Intersection loadIntersection(String fileName){
        File readability = new File(fileName + ".itr");
        Intersection intersection = null;

        if (readability.isFile() && readability.canRead()) {
            try {
                InputStream is = new FileInputStream(fileName + ".itr");
                intersection = (Intersection) DataTools.deserialize(is.readAllBytes());
                System.out.println("Intersection file loaded");
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("File not found, exiting");
            System.exit(1);
        }

        return intersection;
    }

    //save intersection to file
    public static void saveIntersection(String fileName, Intersection intersection) {
        try {
            OutputStream os = new FileOutputStream(fileName + ".itr");
            os.write(Objects.requireNonNull(DataTools.serialize(intersection)));
            os.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    //change human request
    public static synchronized void setLine(String newLine) {
        line = newLine;
    }

    //get human request
    public static synchronized String getLine() {
        return line;
    }

    //receive data from sensors
    public static double[] getSensors(int sensors, int steps) {
        //place for code loading and interpreting sensors data
        return FakeSensors.getReadings(sensors, steps);
    }

    //send information to traffic light controller
    public static void sendToController(State state) {
        // place for code sending green lights orders to controller
        System.out.println(state.stateToString());
    }
}